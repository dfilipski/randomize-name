# Randomize Name

A shell extension written with Sharpshell to randomize the name of a file.

## Install Instructions
- Open Project in Visual Studio 2022 (Likely possible without Visual Studio)
- Right click the RandomizeName project and select properties.
- Select Build and the Strong Naming
- Update the path to the strong name key file found 
- Build the Project
- Using the [SharpShell Server Manager](https://github.com/dwmkerr/sharpshell/releases/download/v2.7.2.0/ServerManager.zip), and the instructions found on [CodeProject](https://www.codeproject.com/Articles/512956/NET-Shell-Extensions-Shell-Context-Menus) Install and register the shell context menu.
