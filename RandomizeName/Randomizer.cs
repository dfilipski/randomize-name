﻿using System;
using System.IO;
using System.Text;

namespace RandomizeName
{
    public class Randomzer
    {
        public Random RandomGen { get; set; }
        const string _validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        public Randomzer()
        {
            RandomGen = new Random(((int)DateTime.Now.Ticks));
        }

        private string GetRandomString(int length)
        {
            var stringBuilder = new StringBuilder();

            for (int i = 0; i < length; i++)
            {
                stringBuilder.Append(_validChars[RandomGen.Next() % _validChars.Length]);
            }

            return stringBuilder.ToString();
        }

        public void RenameFileWithRandomString(string pathToFile, int length = 16)
        {
            var fileInfo = new FileInfo(pathToFile);
            
            File.Move(fileInfo.FullName, $"{fileInfo.Directory}\\{GetRandomString(length)}{fileInfo.Extension}");
        }

    }
}