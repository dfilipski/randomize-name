﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using SharpShell.SharpContextMenu;
using System.Runtime.InteropServices;
using SharpShell.Attributes;

namespace RandomizeName
{
    [ComVisible(true)]
    [COMServerAssociation(AssociationType.AllFiles)]
    public class RandomizeNameExtension : SharpContextMenu
    {
        protected override bool CanShowMenu()
        {
            return true;
        }

        protected override ContextMenuStrip CreateMenu()
        {
            // Create Menu Strip
            var menu = new ContextMenuStrip();

            // Create a "Randomize Name" item
            var itemRandomizename = new ToolStripMenuItem
            {
                Text = "Randomize Name"
            };

            // When we click "Randomize Name" we should call RandomizeNamesWrapper
            itemRandomizename.Click += (sender, args) => RandomizeNameWrapper();

            // Add items to the context menu
            menu.Items.Add(itemRandomizename);

            return menu;
        }

        private void RandomizeNameWrapper()
        {
            var randomizer = new Randomzer();

            foreach (var filePath in SelectedItemPaths)
            {
                randomizer.RenameFileWithRandomString(filePath);
            }
        }
    }
}
