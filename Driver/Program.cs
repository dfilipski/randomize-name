﻿using RandomizeName;

namespace Driver
{
    class Program
    {
        static void Main(string[] args)
        {
            var randomizeName = new Randomzer();

            string path;
            if (args.Length > 0)
                path = args[0];
            else
                path = "C:\\Danny\\test\\file.txt";


            randomizeName.RenameFileWithRandomString(path);
        }
    }
}